###################
Lend System Epson
###################

Lend System Epson is a Web Application for management people lend inventory in PT. Epson Indonesia. This Development use Codeigniter Framework to build web sites using PHP. Its goal is to enable admin to manage employee, inventory, and lend. And the goal for employee to make simple flow lend system. They will have notification on system lend. Develop this project below 3 days, so it's still must to development again next.

*******************
Release Information
*******************

This repo contains code for lend system basic. Not complete code, to protection.

*******************
Server Requirements
*******************

- PHP version 7.0.6 or newer is recommended.

It should work on 7.0.6 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

- MYSQL DBMS
- Bootstrap
- Font Awesome
- Ionicons
- Admin LTE

*******
License
*******

Free.